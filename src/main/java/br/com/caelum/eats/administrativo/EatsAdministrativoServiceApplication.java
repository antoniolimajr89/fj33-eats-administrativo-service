package br.com.caelum.eats.administrativo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class EatsAdministrativoServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EatsAdministrativoServiceApplication.class, args);
	}

}
